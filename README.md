# Functional programming with typescript

In this `workshop` we will explore the features of `fp-ts` and how to use them
to write functional code in TypeScript.

We will also create a simple `Tic Tac Toe` game using `fp-ts` to showcase the
features of the library. We will use `Deno` to run the code.

## Table of contents

- [What is functional programming?](#what-is-functional-programming)
- [What is TypeScript?](#what-is-typescript)
- [Why functional programming with TypeScript?](#why-functional-programming-with-typescript)
- [Prerequisites](#prerequisites)
- [Getting started](#getting-started)
  - [What is fp-ts?](#what-is-fp-ts)
    - [Features](#features)
      - [Option](#option)
      - [Either](#either)
      - [TaskEither](#taskeither)
      - [IO](#io)
  - [Tic Tac Toe](#tic-tac-toe)
- [Conclusion](#conclusion)

## What is functional programming?

Functional programming is a programming paradigm that treats computation as the
evaluation of mathematical functions and avoids changing-state and mutable data.
It is a declarative programming paradigm, which means programming is done with
expressions or declarations instead of statements.

Here's a simple example of this paradigm in Haskell:

```haskell
-- Haskell
add :: Int -> Int -> Int
add a b = a + b
```

But we will not be using Haskell in this repository. We will be using TypeScript
and fp-ts to write functional code.

```typescript
// TypeScript
import { function as func } from "npm:fp-ts";

const result = func.pipe(
  1,
  (x) => x + 1,
);

console.log(result);
```

## What is TypeScript?

TypeScript is a superset of JavaScript that adds static types to the language.
TypeScript is designed for development of large applications and transcompiles
to JavaScript. As TypeScript is a superset of JavaScript, existing JavaScript
programs are also valid TypeScript programs.

Here's a code snippet in JavaScript, and the equivalent in TypeScript:

```javascript
// JavaScript
function add(a, b) {
  return a + b;
}
```

```typescript
// TypeScript
function add(a: number, b: number): number {
  return a + b;
}
```

As you can see, the TypeScript version of the function `add` has type
annotations for the parameters and the return value. This is the main feature
that TypeScript adds to JavaScript.

## Why functional programming with TypeScript?

TypeScript is a great language for functional programming. It has a powerful
type system that can express many of the concepts and patterns used in
functional programming. It also has a rich set of features that make it easy to
write functional code.

# Prerequisites

You will need `Deno` installed and on your path. If you don't have it installed,
run the following command to install [`Deno`](https://deno.land/):

```bash
curl -fsSL https://deno.land/x/install/install.sh | sh
```

You will need to add it to your path. For example, add the following to your
`.bashrc` or `.zshrc`:

```bash
export PATH="$HOME/.deno/bin:$PATH"
```

You will need some familiarity with TypeScript. If you are new to TypeScript,
you can learn the basics from the
[official handbook](https://www.typescriptlang.org/docs/handbook/intro.html).

You will also need a code editor. I recommend using Visual Studio Code with the
`Deno` extension. You can install it from the
[Visual Studio Code Marketplace](https://marketplace.visualstudio.com/items?itemName=denoland.vscode-deno).

# Getting started

## What is fp-ts?

`fp-ts` is a library for typed functional programming in TypeScript. It provides
a set of data types and functions for working with them. It is inspired by
Haskell's `Prelude` and `Data` libraries, and it is designed to be used with
TypeScript's type system.

### Features

#### Option

The `Option` type is used to represent the absence of a value. It is similar to
`Maybe` in Haskell, and `Option` in Scala. It is useful for working with
functions that may not return a value.

With `TypeScript`, we would have to use `null` or `undefined` to represent the
absence of a value. With `fp-ts`, we can use the `Option` type to represent the
absence of a value in a functional way.

Let's see an example. This program will ask the user for a number, and then
print the square of the number. If the user does not enter a number, it will
print an error message.

```typescript
// Non functional way
const input = prompt("enter a number: ");
const number = parseInt(input, 10);

if (isNaN(number)) {
  console.log("You entered an invalid number");
} else {
  console.log(number * number);
}
```

As you can see, this program checks whether the result is `NaN` and prints an
error message if it is. This is not a functional way to handle the absence of a
value.

```typescript
// Functional way
import { function as func, option } from "npm:fp-ts";

const result = func.pipe(
  prompt("enter a number: "),
  option.fromNullable,
  option.map(Number),
  option.filter((n) => !Number.isNaN(n)),
  option.map((n) => n * 2),
  option.match(
    () => "You entered an invalid number",
    (n) => `The result is ${n}`,
  ),
);

console.log(result);
```

The latter program uses the `Option` type to represent the absence of a value in
a functional way. It uses the `fromNullable` function to convert the result of
`prompt` to an `Option`. It uses the `map` function to convert the result to a
number, and the `filter` function to check whether the number is `NaN`. It uses
the `match` function to print an error message if the number is `NaN`, and the
result otherwise.

#### Either

The `Either` type is used to represent a value that can be one of two types. It
is similar to `Either` in Haskell, and `Either` in Scala. It is useful for
working with functions that may return an error.

With `TypeScript`, we would have to use exceptions to represent errors. With
`fp-ts`, we can use the `Either` type to represent errors in a functional way.

Let's see an example. This program will parse a JSON string, and then print the
result. If the JSON string is invalid, it will print an error message.

```typescript
// Non functional way
const parser = (data: string) => {
  try {
    return JSON.parse(data);
  } catch (error) {
    return error;
  }
};

console.log(parser('{"name": "John"}')); // { name: 'John' }
console.log(parser('{"name": "John"')); // SyntaxError: Unexpected end of JSON input
```

```typescript
// Functional way
import { either } from "npm:fp-ts";

const parser = (data: string) => {
  const result = either.tryCatch(
    () => JSON.parse(data),
    (error) => error,
  );
  return result;
};

console.log(parser('{"name": "John"}')); // Right { name: 'John' }
console.log(parser('{"name": "John"')); // Left SyntaxError: Unexpected end of JSON input
```

The latter program uses the `Either` type to represent errors in a functional
way. It uses the `tryCatch` function to parse the JSON string, and return a
`Right` value if it succeeds, and a `Left` value if it fails.

#### TaskEither

The `TaskEither` type is used to represent a computation that may fail or
succeed. It is similar to `TaskEither` in Haskell, and `TaskEither` in Scala. It
is useful for working with asynchronous code that may fail.

With `TypeScript`, we would have to use callbacks or promises to work with
asynchronous code that may fail. With `fp-ts`, we can use the `TaskEither` type
to work with asynchronous code that may fail in a functional way.

```typescript
// Non functional way
const fetchUser = (id, callback) => {
  fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
    .then((response) => {
      if (!response.ok) {
        throw new Error("User not found");
      }
      return response.json();
    })
    .then((user) => {
      callback(null, user);
    })
    .catch((error) => {
      callback(error, null);
    });
};

fetchUser(1, (error, user) => {
  if (error) {
    console.log(error.message);
  } else {
    console.log(user);
  }
});
```

```typescript
// Functional way
import { function as func, taskEither } from "npm:fp-ts";

const result = await func.pipe(
  taskEither.tryCatch(
    () =>
      fetch("https://jsonplaceholder.typicode.com/todos/1").then((res) =>
        res.json()
      ),
    (reason) => new Error(String(reason)),
  ),
  taskEither.match(
    (err) => `Error: ${err}`,
    (value) => `Value: ${JSON.stringify(value)}`,
  ),
)();

console.log(result);
```

The latter program uses the `TaskEither` type to work with asynchronous code
that may fail in a functional way. It uses the `tryCatch` function to fetch a
user from the JSONPlaceholder API, and return a `Right` value if it succeeds,
and a `Left` value if it fails.

#### IO

The `IO` type is used to represent a computation that has side effects. It is
similar to `IO` in Haskell, and `IO` in Scala. It is useful for working with
side effects in a functional way.

With `TypeScript`, we would have to use functions that have side effects. With
`fp-ts`, we can use the `IO` type to work with side effects in a functional way.

```typescript
// Non functional way
const print = (message: string) => {
  console.log(message);
};

print("Hello, world!");
```

```typescript
// Functional way
import { io } from "npm:fp-ts";

const print = (message: string) => io.of(console.log(message));

print("Hello, world!")();
```

The latter program uses the `IO` type to work with side effects in a functional
way. It uses the `of` function to create an `IO` value that prints a message to
the console.

## Tic Tac Toe

We will create a simple `Tic Tac Toe` game using `fp-ts` to showcase the
features of the library.

The unfinished game is in the `game.ts` file. This file contains all the needed
function prototypes and types to implement the game.

Please do not change the function prototypes and types in the `game.ts` file.
This would break the main driver game logic.

> Take a look at how methods such as `filter`, `map`, `chain`, `fold` and
> `match` work. They are your best friends when working with `fp-ts`.

# Conclusion

The main advantage of functional programming with TypeScript is that it allows
you to write code that is more concise, more predictable, and easier to test.

It also allows you to write code that is easier to refactor, because it is
easier to reason about the behavior of the code. This is because functional
programming encourages you to write code that is more modular, and less
dependent on the state of the program.

In this repository, we will explore the features of `fp-ts` and how to use them
to write functional code in TypeScript.
