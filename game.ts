import chalk from "chalk";
import { function as func, io, option } from "fp-ts";

type Player = "X" | "O";
type Cell = Player | null;
type Board = Cell[];

/**
 * This function returns an empty board of 9 cells
 * represented by an array of null values.
 */
const initBoard: io.IO<Board> = () => {
  // Your code goes here
  return [];
};

/**
 * This function returns the color for a given
 * cell in the board.
 * 
 * If the cell is empty, it should `chalk.dim` the index plus one
 * If the cell is "X", it should `chalk.green` the cell
 * If the cell is "O", it should `chalk.red` the cell
 */
const color = (board: Board, index: number): string => {
  const square = board[index];
  return square === "X"
    ? chalk.green(square)
    : square === "O"
    ? chalk.red(square)
    : chalk.dim(index + 1);
};

/**
 * This displays the board in the console.
 *
 * It should display the board with the
 * current state of the game and the
 * position of each cell.
 *
 * Colors are not mandatory but it's a nice
 * touch to differentiate the players.
 */
const displayBoard: (board: Board) => io.IO<void> = (board) => () => {
  // Your code goes here
};

/**
 * This function should validate if the move
 * is valid based on the current board
 * and the position
 *
 * The move is valid if the position is not taken
 * and it's within the board range (0-8)
 *
 * Return true if the move is valid, false otherwise
 */
const isValidMove: (board: Board, pos: number) => boolean = (board, pos) => {
  // Your code goes here
  return false;
};

/**
 * This array contains the winning positions
 * for the game.
 *
 * Use it to check if the current player has
 * a winning combination.
 */
const winningPositions: number[][] = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

/**
 * This function makes use of the `displayBoard`
 * function to display the current state of the
 * game.
 */
const display = (board: Board) => {
  // Your code goes here
};

/**
 * This function asks for input from the user
 * and validates it using the `isValidMove` function.
 *
 * It should return the position the player wants to
 * place their mark.
 *
 * If the move is not valid, it should ask for input again.
 *
 * The input should be a number from 1 to 9 representing
 * the position in the board.
 *
 * Use `option` from `fp-ts` to handle the input and
 * validation.
 *
 * Take a look at `<Option>.fromNullable`, `<Option>.map`,
 * `<Option>.filter` and `<Option>.match` to handle the input
 * and validation.
 *
 * https://gcanti.github.io/fp-ts/modules/Option.ts.html
 * https://medium.com/@miclau2004/using-option-with-fp-ts-in-your-typescript-code-f340d496acd7
 */
const askForInput = (board: Board, player: Player): number => {
  // Your code goes here
  return -1;
};

/**
 * This function returns a new board with the
 * player's move.
 */
const makeMove: (board: Board, pos: number, player: Player) => Board = (
  board,
  pos,
  player,
) => {
  // Your code goes here
  return board;
};

/**
 * This function checks if the move is a winning move
 * based on the current state of the board.
 *
 * It should return true if the move is a winning move,
 * false otherwise.
 *
 * The winning positions are defined in the `winningPositions`
 * array.
 */
const isWinningMove: (board: Board, pos: number, player: Player) => boolean = (
  board,
  pos,
  player,
) => {
  // Your code goes here
  return false;
};

/**
 * This function checks if the game is a draw.
 *
 * It should return true if the game is a draw,
 * false otherwise.
 *
 * A game is a draw if there are no more moves
 * available and there's no winner.
 */
const isDraw: (board: Board) => boolean = (board) => {
  // Your code goes here
  return false;
};

/**
 * This is the main function of the game.
 *
 * You do not need to modify this function.
 *
 * If you feel adventurous, you can try to
 * refactor it using even more functional programming
 * concepts.
 */
const loop = () => {
  let board = initBoard();
  let player: Player = "X";
  let winner: Player | null = null;

  while (!winner && !isDraw(board)) {
    display(board);
    const pos = askForInput(board, player);
    board = makeMove(board, pos, player);
    if (isWinningMove(board, pos, player)) {
      winner = player;
    }
    player = player === "X" ? "O" : "X";
  }

  display(board);
  console.log(
    winner
      ? `Player ${chalk.magenta(winner)} wins!`
      : chalk.dim("It's a draw!"),
  );
};

loop();
